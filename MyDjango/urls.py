from django.conf.urls import url, include
from django.contrib import admin

from . import views
from homepage import views as homepage_views



urlpatterns = [
    # url(r'^login/$', views.login),
    url(r'^login/', homepage_views.login_view, name='login_my'),
    url(r'^logout/', homepage_views.logout_view),
    url(r'^admin/', admin.site.urls),
    url(r'^person/', include('person.urls')),
    url(r'^location/', include('location.urls')),
    url(r'^organization/', include('organization.urls')),
    url(r'^$', views.index),
    url(r'^mediatag/$', views.mediatag),
]