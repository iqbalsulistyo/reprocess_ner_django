from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, logout, login
# from django.contrib.auth.decorators import login_required
# from django.conf import settings
from person.models import Document
from person.forms import DocumentForm
from django.core.urlresolvers import reverse


# def login(request):
# 	return render(request,'login.html')

# def logout(request):
#     logout(request)
#     return redirect('login/')


# @login_required(login_url=settings.LOGIN_URL)
def index(request):
	context = {
		'title' : 'Tools Operational',
		'heading' : 'Reprocess Data IMA',
		'subheading' : 'di Kelas Terbuka',
	}
	documents = Document.objects.all()
	# return render(request,'index.html',context)
	reverse('login_my')
	return render(request, 'index.html', {
        'context': context, 
        'documents':documents
    })
    


def mediatag(request):
	return render(request,'mediatag.html')

# from django.shortcuts import render, redirect, HttpResponseRedirect
# from django.core.urlresolvers import reverse
# from django.conf import settings
# from django.core.files.storage import FileSystemStorage
# from person.models import Document
# from person.forms import DocumentForm

# # Create your views here.
# def idhash(request):
#     if request.method == 'POST':
#         form = DocumentForm(request.POST, request.FILES)
#         if form.is_valid():
#             try: 
#                 form.save()
#                 return HttpResponseRedirect(reverse('Successfully Save Data'))
#             except:
#                 pass
#     else:
#         form = DocumentForm()
#     documents = Document.objects.all()
#     return render(request, 'person/idhash.html', {
#         'form': form, 'documents':documents
#     })