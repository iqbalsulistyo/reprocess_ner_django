from django.shortcuts import render, redirect, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.conf import settings
from django.core.files.storage import FileSystemStorage

# from django.contrib.auth import authenticate, login, logout
# from django.contrib.auth.decorators import login_required
# from django.conf import settings

from person.models import Document
from person.forms import DocumentForm
# from django.contrib import messages


# @login_required
def idhash(request):
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            try: 
                form.save()
                # messages.add_message(request, messages.INFO, 'Successfully Save Data!!!')
                # return HttpResponseRedirect(reverse('Successfully Save Data'))
            except:
                pass
                # messages.add_message(request, messages.INFO, 'Failed Save Data!!!')
    else:
        form = DocumentForm()
    documents = Document.objects.all()
    return render(request, 'person/idhash.html', {
        'form': form, 'documents':documents
    })


def name(request):
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES, request.POST)
        if form.is_valid():
            form.save()
    else:
        form = DocumentForm()
    return render(request, 'person/name.html', {
        'form': form
    })


def crud(request):
    return render(request,'person/crud.html')