from __future__ import unicode_literals
from django.db import models


class Document(models.Model):
	ner = models.CharField(max_length=100)
	category = models.CharField(max_length=20)
	filename = models.FileField(upload_to='')
	created_at = models.DateTimeField(auto_now_add=True)
	status = models.CharField(max_length=100)

	#for use object on get data database
	objects = models.Manager() 
	class Meta:  
		db_table = "queue"