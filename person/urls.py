from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
	url(r'^idhash/$', views.idhash),
	url(r'^name/$', views.name),
	url(r'^crud/$', views.crud),
]
