# -*- coding: utf-8 -*-
# Generated by Django 1.11.27 on 2020-03-03 02:43
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0007_auto_20200303_0242'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='document',
            name='rangedate',
        ),
    ]
