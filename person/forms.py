from django import forms
from person.models import Document

class DocumentForm(forms.ModelForm):
    ner = forms.CharField(widget = forms.TextInput(attrs={'readonly':'readonly'}), initial='Person')
    CHOICES = (('master', 'Master'),('blacklist', 'Blacklist'))
    category = forms.ChoiceField(choices=CHOICES)
    status = forms.CharField(widget = forms.HiddenInput(), required=False, initial='On Progress')
    class Meta:
        model = Document
        # fields = "__all__"
        fields = ['ner','category','filename','status']
