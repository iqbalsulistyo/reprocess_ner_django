from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, logout, login
from django.contrib import messages

from useraccess.models import Akun

# Create your views here.

def login_view(request):
    if request.POST:
        user = authenticate(username=request.POST['username'], password=request.POST['password'])
        if user is not None:
            if user.is_active:
                try:
                    akun = Akun.objects.get(akun=user.id)
                    login(request, user)

                    # request.session['karyawan_id'] = akun.karyawan_id
                    # request.session['jenis_akun'] = akun.jenis_akun
                    # request.session['username'] = request.POST['username']
                except:
                    pass
                return redirect('/')
            else:   
                messages.add_message(request, messages.INFO, 'You have not registered, please contact admin')
        else:
            messages.add_message(request, messages.INFO, 'Incorrect Username or Password')
    return render(request, 'login.html')

def logout_view(request):
    logout(request)
    return redirect('/login/')