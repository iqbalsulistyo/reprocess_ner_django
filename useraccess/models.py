from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User

class Akun (models.Model):
    # JENIS_AKUN_CHOICES = (
    #     ('karyawan', 'Karyawan'),
    #     ('admin', 'Administrator'),
    # )

    akun = models.ForeignKey(User)
    # karyawan = models.ForeignKey(Karyawan)
    # jenis_akun = models.CharField(max_length=20, choices=JENIS_AKUN_CHOICES)

    def __unicode__(self):
        return self.akun.username