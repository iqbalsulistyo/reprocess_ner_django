from django.shortcuts import render, redirect, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from organization.models import Document
from organization.forms import DocumentForm
# from django.contrib.auth.decorators import login_required
# from django.conf import settings


# @login_required(login_url=settings.LOGIN_URL)
def idhash(request):
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            try: 
                form.save()
                return HttpResponseRedirect(reverse('Successfully Save Data'))
            except:
                pass
    else:
        form = DocumentForm()
    documents = Document.objects.all()
    return render(request, 'organization/idhash.html', {
        'form': form, 'documents':documents
    })


def name(request):
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES, request.POST)
        if form.is_valid():
            form.save()
    else:
        form = DocumentForm()
    return render(request, 'organization/name.html', {
        'form': form
    })


def crud(request):
    return render(request,'organization/crud.html')