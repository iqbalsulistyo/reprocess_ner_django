from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^idhash/$', views.idhash),
	url(r'^name/$', views.name),
]